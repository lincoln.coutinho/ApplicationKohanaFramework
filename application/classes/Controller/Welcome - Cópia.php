<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Welcome extends Controller_Template {
	public $template = 'padrao';

	public function action_index()
	{
	 $view = View::Factory('login/formLogin');
     $this->template->content = $view;

	}
	
	public function action_login()
	{

	 if (Session::instance()->get('usuario') != NULL) {

            // Já logado vai para index
            $this->request->redirect(URL::base());
        }
		else{
	
	 $usuariodb = ORM::Factory('usuario')
                            ->where('email', '=', $_POST['usuario'])
                            ->where('senha', '=', $_POST['senha'])->find();
							
							
	Session::instance()->set('usuario',$usuariodb->email);
	Session::instance()->set('senha',$usuariodb->senha);
	

		$view = View::factory('login/formWelcome');
        $view->set('nomelogado',$usuariodb->nome);
        $view->set('senhalogado',Session::instance()->get('senha'));
        $view->set('emaillogado',$usuariodb->email);
        $view->render();
		$this->template->content = $view;
		}
	}

} // End Welcome
