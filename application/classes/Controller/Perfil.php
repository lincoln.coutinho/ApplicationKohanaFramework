<?php
class Controller_Perfil extends Controller_Template{
	
	public $template = 'padrao';

	public function action_index(){
	
	$this->action_lista();
		
	}
	
	public function action_incluir(){
	$this->reponse->content = "Teste";
	}
	
	public function action_ver(){

	    $id = Request::current()->param('id');

        $usuario = ORM::factory('usuario', $id);
        $mensagens = ORM::Factory('mensagem')
                ->where('usuario_id_rem', '=', $id)
                ->or_where('usuario_id_dest', '=', $id)
                ->order_by('datcad', 'asc')
                ->order_by('id', 'asc')
                ->find_all();

        $view = View::factory('perfil/verPerfil');
        $view->set('usuario', $usuario);
        $view->set('mensagens', $mensagens);

        $this->template->content = $view;
	
	
	}
	
	public function action_deletar(){
	$this->reponse->content = "Teste";
	}
	
	
	public function action_lista(){
	

	$usuarios = ORM::Factory('usuario')
	->find_all();

	$view = View::factory('perfil/listaPerfil');
	$view->set('usuarios', $usuarios);

	$this->template->content = $view;
		
		
	}
}
?>
