<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Welcome extends Controller_Template {
	public $template = 'padrao';

	public function action_index()
	{
	
	
		if(Session::instance()->get('usuario') == NULL){

				$view = View::Factory('login/formLogin');
				$this->template->content = $view;

		}
		else{
		
		$this->redirect('../processo/listar');


		}

	}
	
	public function action_login()
	{

			if ($_SESSION['usuario'] != NULL) {

					$this->redirect('../welcome/index');

			}
			else{

					$usuariodb = ORM::Factory('login')
					->where('usuario', '=', $_POST['usuario'])
					->where('senha', '=', $_POST['senha'])->find();

					$_SESSION['usuario'] = $usuariodb->usuario;

					$_SESSION['senha'] = $usuariodb->senha;

					$_SESSION['nome'] = $usuariodb->nome;

					$_SESSION['email'] = $usuariodb->usuario;
					
					$_SESSION['id'] = $usuariodb->id;
					
					$_SESSION['depto'] = $usuariodb->Departamento;

					$this->redirect('../welcome/index');


			}
	}

} // End Welcome
