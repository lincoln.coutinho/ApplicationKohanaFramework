﻿<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Processo extends Controller_Template {
	public $template = 'padrao';

	

	public function action_index()
	{
	$this->action_listar();

	}
	
	public function action_incluir()
	{

	}
	
	public function action_listar()
	{

			
			function pegaInteressado($id,$outro){
			
			if($id == 'outros' OR $id == 'outro'){

			$processos = ORM::Factory('outrointeressado')
			->where('id', '=', $outro)
			->find();

			$nome_interessado = $processos->outro_interessado;

			}
			else{
			$processos = ORM::Factory('login')
			->where('id', '=', $id)
			->find();

			$nome_interessado = $processos->nome;
			}
			return $nome_interessado;
			}


			
			function getAssunto($id,$outro){
			
			$processos = ORM::Factory('assunto')
			->where('id', '=', $id)
			->find();

			$assunto = $processos->assunto;
				
			if($assunto == 'Outros' OR $assunto == 'Outro'){

			$processos = ORM::Factory('outroassunto')
			->where('id', '=', $outro)
			->find();

			$assunto = $processos->outro_assunto;

			}

			return $assunto;
			}
			
			
			
			$localiza = Session::instance()->get('id');
			$processos = ORM::Factory('processo')
			->where('localiza', '=', $localiza)
			->find_all();


			$view = View::factory('processo/listarProcessos');
			$view->set('processos', $processos);
			$this->template->titulo_page = "Meus Processos";
			$this->template->nome_usuario = Session::instance()->get('nome');
			$this->template->content = $view;
	
	
	
	}
	public function action_editar()
	{

	}
	public function action_etiqueta()
	{
	
			function pegaInteressado($id,$outro){
			
			if($id == 'outros' OR $id == 'outro'){

			$processos = ORM::Factory('outrointeressado')
			->where('id', '=', $outro)
			->find();

			$nome_interessado = $processos->outro_interessado;

			}
			else{
			$processos = ORM::Factory('login')
			->where('id', '=', $id)
			->find();

			$nome_interessado = $processos->nome;
			}
			return $nome_interessado;
			}
	
			function getAssunto($id,$outro){

				$processos1 = ORM::Factory('assunto')
				->where('id', '=', $id)
				->find();

				$assunto = $processos1->assunto;

				if($assunto == 'Outros' OR $assunto == 'Outro'){
				$processos = ORM::Factory('outroassunto')
				->where('id', '=', $outro)
				->find();

				$assunto = $processos->outro_assunto;

				}

				return $assunto;
		}
		
		$id = Request::current()->param('id');
		$etiquetas = ORM::Factory('processo')
		->where('processo', '=', $id)
		->find();

		$view = View::factory('processo/etiquetaProcessos');
		$view->set('processo', $etiquetas->processo);
		$view->set('data', date("d/m/Y", strtotime($etiquetas->data)));
		$view->set('interessado', pegaInteressado($etiquetas->interessado,$etiquetas->outro_interessado));
		$view->set('assunto', getAssunto($etiquetas->assunto,$etiquetas->outro_assunto));
		$this->template->titulo_page = "Processo nº ".$id;
		$this->template->nome_usuario = Session::instance()->get('nome');
		$this->template->content = $view;

	}
	public function action_pesquisar()
	{

	}
	public function action_encaminhar()
	{
		if($_POST['encaminhar'] == 'true'){
		
				$processo = $_POST['idProcesso'];
				$usuario = Session::instance()->get('id');
				$texto = $_POST['texto'];
				$data = date();
				$hora = date('H:i');
				$destino = $_POST['destino'];
				
				$queryInsert = DB::insert('expediente_caminho', array('processo', 'usuario', 'texto', 'data', 'hora', 'localiza'))->values(array($processo, $usuario, $texto, $data, $hora, $destino));

				$resultInsert = $queryInsert->execute();
		
				$queryUpdate = DB::update('expediente_dados')->set(array('localiza' => $destino))->where('processo', '=', $processo);

				$resultUpdate = $queryUpdate->execute();

				$cont = 0;
				
				foreach($_FILES['Arquivo']['tmp_name'] as $key => $tmp_name ){
				
				$file_name = $_FILES['Arquivo']['name'][$key];

				$ext = strtolower(end(explode(".", $file_name)));

				$file_tmp =$_FILES['Arquivo']['tmp_name'][$key];
				
				$file_type=$_FILES['Arquivo']['type'][$key];	

				$file_name = md5($file_name);
				
				$file_name = $file_name.'.'.$ext;

				//$query= "INSERT INTO `anexo_andamento`(`nome_anexo`, `id_andamento`) VALUES ('$file_name',$id_ultimo_andamento)";

				$desired_dir="upload_anexo/";
				
				if(empty($errors)==true){
				
				if($ext <> ''){
				move_uploaded_file($file_tmp,"$desired_dir/".$file_name);
		

				}
				
				}else{
				
				print_r($errors);
				
				}
				
				$cont++;
				
				}
			
								
				$sucess =  '<div class="alert alert-dismissable alert-success">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>Encaminhado com exito'.$type.'!</strong>
                </div>';
				
				$view = View::factory('processo/mensagemProcessos');
				$view->set('mensagem', $sucess);
				$this->template->titulo_page = "Encaminhado";
				$this->template->nome_usuario = Session::instance()->get('nome');
				$this->template->content = $view;
		
		}
		else{
				$id = Request::current()->param('id');
				
				$destinos = ORM::Factory('login')
				->order_by('nome', 'asc')
				->find_all();
				
				$view = View::factory('processo/encaminharProcessos');
				$view->set('destinos', $destinos);
				$view->set('idProcesso', $id);
				$this->template->titulo_page = "Encaminhar nº ".$id;
				$this->template->nome_usuario = Session::instance()->get('nome');
				$this->template->content = $view;
		}

	}
	
	public function action_finalizar()
	{
		$processo = Request::current()->param('id');
		
		$conclusao = $_POST['texto'];
		
		if($conclusao == null){
		
		$view = View::factory('processo/finalizarProcessos');
		
		$view->set('idProcesso', $processo);
		
		$this->template->titulo_page = "Finalizar Processo nº".$processo;
		
		$this->template->nome_usuario = Session::instance()->get('nome');
		
		$this->template->content = $view;
	
	}
	else{
	
		$processo = $_POST['idProcesso'];
		
		$data_conc = date("Y-m-d H:i:s");
		
		$queryUpdate = DB::update('expediente_dados')->set(array('conclusao' => $conclusao,'usuario_conclusao' => Session::instance()->get('id'),'data_conclusao' => $data_conc,'status' => 2))->where('processo', '=', $processo);

		$resultUpdate = $queryUpdate->execute();
		

		$sucess =  '<div class="alert alert-dismissable alert-success">
		<button type="button" class="close" data-dismiss="alert">×</button>
		<strong>Finalizado com exito!</strong>
		</div>';
		
		$view = View::factory('processo/mensagemProcessos');
		$view->set('mensagem', $sucess);
		$this->template->titulo_page = "Finalizado";
		$this->template->nome_usuario = Session::instance()->get('nome');
		$this->template->content = $view;

	}
	



	}
	
	public function action_ver()
	{
	
	
		function pegaInteressado($id,$outro){

				if($id == 'outros' OR $id == 'outro' OR $id == 'Outros' OR $id == 'Outro'){

				$processos = ORM::Factory('outrointeressado')
				->where('id', '=', $outro)
				->find();

				$nome_interessado = $processos->outro_interessado;

				}
				else{
				$processos = ORM::Factory('login')
				->where('id', '=', $id)
				->find();

				$nome_interessado = $processos->nome;
				}
				return $nome_interessado;
		}
		
		
		function getSubClasse($id){
				
				$subclasses = ORM::Factory('subclasse')
				->where('id', '=', $id)
				->find();
				$id_classe = $subclasses->id_classes;
				
				$classes = ORM::Factory('classe')
				->where('id', '=', $id_classe)
				->find();
				

				return $classes->classes.' - '.$subclasses->subclasse;
		}
		
		function formatHour($data){
				$newData = '';
				for($i = 10;$i<=18;$i++){
				$newData = $newData.$data[$i];
				}
				return $newData;
		}



		function getAssunto($id,$outro){

				$processos = ORM::Factory('assunto')
				->where('id', '=', $id)
				->find();

				$assunto = $processos->assunto;

				if($assunto == 'Outros' OR $assunto == 'Outro'){

				$processos = ORM::Factory('outroassunto')
				->where('id', '=', $outro)
				->find();

				$assunto = $processos->outro_assunto;

				}

				return $assunto;
		}

		$id = Request::current()->param('id');

		$processos = ORM::Factory('processo')
		->where('processo', '=', $id)
		->find();


		$andamentos = ORM::Factory('andamento')
		->where('processo', '=', $id)
		->order_by('id', 'asc')
		->find_all();

		$view = View::factory('processo/verProcessos');
		$view->set('processos', $processos);
		$view->set('andamentos', $andamentos);
		$view->set('idProcesso', $id);
		$this->template->titulo_page = "Processo nº ".$id;
		$this->template->nome_usuario = Session::instance()->get('nome');
		$this->template->content = $view;
	
	}

	
		public function action_imprimir()
	{
	
	
		function pegaInteressado($id,$outro){

				if($id == 'outros' OR $id == 'outro' OR $id == 'Outros' OR $id == 'Outro'){

				$processos = ORM::Factory('outrointeressado')
				->where('id', '=', $outro)
				->find();

				$nome_interessado = $processos->outro_interessado;

				}
				else{
				$processos = ORM::Factory('login')
				->where('id', '=', $id)
				->find();

				$nome_interessado = $processos->nome;
				}
				return $nome_interessado;
		}
		
		
		function getSubClasse($id){
				
				$subclasses = ORM::Factory('subclasse')
				->where('id', '=', $id)
				->find();
				$id_classe = $subclasses->id_classes;
				
				$classes = ORM::Factory('classe')
				->where('id', '=', $id_classe)
				->find();
				

				return $classes->classes.' - '.$subclasses->subclasse;
		}
		
		function formatHour($data){
				$newData = '';
				for($i = 10;$i<=18;$i++){
				$newData = $newData.$data[$i];
				}
				return $newData;
		}
		
		function getYear($data){
				$newData = '';
				for($i = 0;$i<=3;$i++){
				$newData = $newData.$data[$i];
				}
				return $newData;
		}



		function getAssunto($id,$outro){

				$processos = ORM::Factory('assunto')
				->where('id', '=', $id)
				->find();

				$assunto = $processos->assunto;

				if($assunto == 'Outros' OR $assunto == 'Outro'){

				$processos = ORM::Factory('outroassunto')
				->where('id', '=', $outro)
				->find();

				$assunto = $processos->outro_assunto;

				}

				return $assunto;
		}

		$id = Request::current()->param('id');

		$processos = ORM::Factory('processo')
		->where('processo', '=', $id)
		->find();


		$andamentos = ORM::Factory('andamento')
		->where('processo', '=', $id)
		->order_by('id', 'asc')
		->find_all();

		$view = View::factory('processo/imprimirProcessos');
		$view->set('processos', $processos);
		$view->set('andamentos', $andamentos);
		$view->set('idProcesso', $id);
		$this->template->titulo_page = "Processo nº ".$id.' / '.getYear($processos->data);
		$this->template->nome_usuario = Session::instance()->get('nome');
		$this->template->content = $view;
	
	}

} // End Welcome
