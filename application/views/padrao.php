<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Administrador de Processos</title>
    <link rel="stylesheet" href="../../../AdmProcessKohana/media/css/bootstrap.css" media="screen">
    <link rel="stylesheet" href="../../../AdmProcessKohana/media/css/bootswatch.min.css">
<script type="text/javascript" src="../../../AdmProcessKohana/media/js/jquery-1.3.2.min.js"></script>
<style>
.geral {
margin: 0 auto;
width: 990px;
}
</style>
<script>
function carregaBusca(url,id) {
    $("div#"+id).html("<font color=\"#FF0000\">Carregando ...</font>  <img src='imagens/aguarde.gif' align='top' alt='aguarde' />");
            $.post(url,{ }
            ,function(retorno){$("#"+id).html(retorno)});
}
</script>
<!-- TinyMCE -->
<script type="text/javascript" src="../media/js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
	tinyMCE.init({
		// General options
		mode : "textareas",
		theme : "advanced",
		plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups",

		// Theme options
		theme_advanced_buttons1 : "bold,italic,underline,|,justifyleft,justifycenter,justifyright,justifyfull,|,fullscreen",
		theme_advanced_buttons2 : "",
		
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,

		// Replace values for the template plugin
		template_replace_values : {
			username : "Some User",
			staffid : "991234"
		}
	});
</script>
<!-- /TinyMCE -->
</head>
<body>
<div class="geral">
<?php
$etiqueta = strpos($_SERVER['REQUEST_URI'], 'etiqueta');
$imprimir = strpos($_SERVER['REQUEST_URI'], 'imprimir');
if(Session::instance()->get('nome') != NULL and $etiqueta == 0 and $imprimir == 0){
?>
<div class="navbar navbar-default">
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="#"><?=$nome_usuario?></a>
                </div>
                <div class="navbar-collapse collapse navbar-responsive-collapse">
                  <ul class="nav navbar-nav">
                   
                    <li><a href="processo/pesquisar">Pesquisar</a></li>
                    <li><a href="../../../AdmProcessKohana/processo/listar">Meus Processos</a></li>
                    <li><a href="incluir.php">Incluir</a></li>
                    

                  </ul>
<ul class="nav navbar-nav navbar-right">
                    <li><a href="sair.php">Sair</a></li>
 
                  </ul>
                </div>
              </div>
	<?php
}
?>
		
	
	<div class="col-lg-12">
            <div class="well bs-component">
 
                <fieldset>
				<?php
if(Session::instance()->get('nome') != NULL){
?>
                  <legend><?=$titulo_page?></legend>
				 				<?php
} 
  ?>
<?=$content?>

                </fieldset>
            
          </div>
          </div>
	
	
  </div>
  <script src="../../../AdmProcessKohana/media/js/jquery-1.10.2.min.js"></script>
    <script src="../../../AdmProcessKohana/media/js/bootstrap.min.js"></script>
    <script src="../../../AdmProcessKohana/media/js/bootswatch.js"></script>
</body>
</html>  