<div id="titulo">
    Usuários
</div>
<div id="corpo">

    Exibindo <?php echo $usuarios->count(); ?> usuários
    <table>
        <thead>
        <th>Nome</th>
        <th width="100">Ação</th>
        </thead>
        <?php
        $i = 0;
        foreach ($usuarios as $usuario) {
            $i++;
            ?>
            <tbody>
                <tr>
                    <td><?php echo $usuario->nome; ?></td>
                    <td class="center"><?php echo html::anchor(URL::base() . '../../perfil/ver/' . $usuario->id, 'Ver Perfil'); ?></td>
                </tr>
            </tbody>
        <?php } ?>
    </table>
</div>

